﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Фадеев_ПрР1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int count = 0;

            Console.Write("Столбцов: ");
            int x = int.Parse(Console.ReadLine());
            Console.Write("Строк: ");
            int y = int.Parse(Console.ReadLine());
            float[,] mas = new float[x, y];
            Console.WriteLine();

            Console.WriteLine("Заполни матрицу");

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    Console.Write("mas[" + i + "," + j + "]: ");
                    mas[i, j] = float.Parse(Console.ReadLine());
                }
            }
            Console.WriteLine();

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    mas[i, j] = (float)Math.Sin(i + j / 2);
                    if (mas[i, j] > 0) count += 1;
                    Console.Write(" [" + i + "," + j + "]: " + mas[i, j] + "\t");
                }
                Console.WriteLine();
            }
            Console.Write("Число положительных элементов: " + count);
            Console.ReadLine();
        }
    }
}
