The program finds out how many positive elements a matrix of dimension n * m contains if a_ij=sin⁡〖(i+j/2)〗.
The program creates a matrix of the specified dimension and fills it with elements according to the specified formula, after which it determines the number of positive elements.

Developer: ©️ Fadeev I.A. 